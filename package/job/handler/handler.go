package handler

import (
	"JobVacancy/lib/log"
	"JobVacancy/lib/response"
	"JobVacancy/package/job/usecase"

	"github.com/labstack/echo/v4"
)

type jobHandler struct {
	uc usecase.JobUsecase
}

func NewJobHandler(uc usecase.JobUsecase) *jobHandler {
	return &jobHandler{uc}
}

func (h *jobHandler) Mount(g *echo.Group) {
	g.GET("/empTypes", h.GetEmpTypes)
	g.GET("/posLevels", h.GetPosLevels)
	g.GET("/jobList", h.GetJobsList)
	g.GET("/jobDetail", h.GetJobDetail)
}

func (h *jobHandler) GetEmpTypes(e echo.Context) error {
	log.Info("Get Employment Types ...")
	data, err := h.uc.GetEmpTypes()
	if err != nil {
		return response.ToJson(e).InternalServerError(err.Error())
	}

	return response.ToJson(e).OK(data, "Get Employment Types Success")
}
func (h *jobHandler) GetPosLevels(e echo.Context) error {
	log.Info("Get Position Levels ...")
	data, err := h.uc.GetPosLevels()
	if err != nil {
		return response.ToJson(e).InternalServerError(err.Error())
	}

	return response.ToJson(e).OK(data, "Get Position Levels Success")
}
func (h *jobHandler) GetJobsList(e echo.Context) error {
	log.Info("Get Jobs ...")
	data, err := h.uc.GetJobsList()
	if err != nil {
		return response.ToJson(e).InternalServerError(err.Error())
	}

	return response.ToJson(e).OK(data, "Get Jobs Success")
}
func (h *jobHandler) GetJobDetail(e echo.Context) error {
	log.Info("Get Job Detail ...")
	id := e.QueryParam("id")
	data, err := h.uc.GetJobDetail(id)
	if err != nil {
		return response.ToJson(e).InternalServerError(err.Error())
	}

	return response.ToJson(e).OK(data, "Get Job Detail Success")
}
