Instalasi Back End
-> Buat file .env yang di dalam folder /app.
-> Isi dari file .env antara lain DB_STRING (untuk connection ke database) dan PORT (port yang digunakan untuk menjalankan backend)
    Contoh:
    DB_STRING='host=localhost user=postgres password=098iop dbname=job_vacancy port=5432 sslmode=disable TimeZone=Asia/Jakarta'
    PORT=8080
-> Masuk ke terminal untuk menjalankan "go mod tidy"
-> Jika proses selesai, maka backend siap dijalankan dengan cara masuk ke folder /app. Kemudian jalankan "go run ."
