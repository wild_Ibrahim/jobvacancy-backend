package main

import (
	"JobVacancy/lib/log"
	"fmt"
	"net/http"
	"os"

	"JobVacancy/config/database"

	jobHandler "JobVacancy/package/job/handler"
	jobRepository "JobVacancy/package/job/repository"
	jobUsecase "JobVacancy/package/job/usecase"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {

	if err := godotenv.Load(".env"); err != nil {
		log.Error(fmt.Sprintf("Failed load .env: %v", err.Error()))
		os.Exit(2)
	}

	dbConn := database.CreateConnection()
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowMethods: []string{http.MethodGet},
	}))

	e.GET("/", func(c echo.Context) error {
		return c.HTML(http.StatusOK, "Hello, Template")
	})

	jobRepo := jobRepository.NewJobRepo(dbConn)
	jobUC := jobUsecase.NewJobUsecase(jobRepo)
	jobHandler.NewJobHandler(jobUC).Mount(e.Group("/job"))

	if err := e.Start(":" + os.Getenv("PORT")); err != nil {
		log.Error(fmt.Sprintf("Failed start echo: %v", err.Error()))
	}
}
