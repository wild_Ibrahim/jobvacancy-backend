package model

type PositionLevel struct {
	Id   int    `json:"id" gorm:"column:pos_level_id"`
	Name string `json:"name" gorm:"column:pos_level_name"`
}

type EmploymentType struct {
	Id   int    `json:"id" gorm:"column:emp_type_id"`
	Name string `json:"name" gorm:"column:emp_type_name"`
}

type JobDetail struct {
	Id                string `json:"id" gorm:"column:job_id"`
	Name              string `json:"name" gorm:"column:job_name"`
	Desc              string `json:"desc" gorm:"column:job_desc"`
	MinQualifications string `json:"min_qualifications" gorm:"column:job_min_qualifications"`
	MinExperiences    string `json:"min_experiences" gorm:"column:job_min_experiences"`
	Skill             string `json:"skill" gorm:"column:job_skill"`
	Benefit           string `json:"benefit" gorm:"column:job_benefit"`
	PosLevelName      string `json:"pos_level_name" gorm:"column:pos_level_name"`
	EmpTypeName       string `json:"emp_type_name" gorm:"column:emp_type_name"`
}

type JobList struct {
	Id   string `json:"id" gorm:"column:job_id"`
	Name string `json:"name" gorm:"column:job_name"`
	Desc string `json:"desc" gorm:"column:job_desc"`
}
