package usecase

import (
	"JobVacancy/package/job/model"
	"JobVacancy/package/job/repository"
)

type jobUsecase struct {
	repo repository.JobRepo
}

func NewJobUsecase(repo repository.JobRepo) JobUsecase {
	return &jobUsecase{repo}
}

type JobUsecase interface {
	GetEmpTypes() ([]model.EmploymentType, error)
	GetPosLevels() ([]model.PositionLevel, error)
	GetJobsList() ([]model.JobList, error)
	GetJobDetail(id string) (model.JobDetail, error)
}

func (uc *jobUsecase) GetEmpTypes() ([]model.EmploymentType, error) {
	return uc.repo.GetEmpTypes()
}
func (uc *jobUsecase) GetPosLevels() ([]model.PositionLevel, error) {
	return uc.repo.GetPosLevels()
}
func (uc *jobUsecase) GetJobsList() ([]model.JobList, error) {
	return uc.repo.GetJobsList()
}
func (uc *jobUsecase) GetJobDetail(id string) (model.JobDetail, error) {
	return uc.repo.GetJobDetail(id)
}
