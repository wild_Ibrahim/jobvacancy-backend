package repository

import (
	"JobVacancy/config/database"
	"JobVacancy/package/job/model"
)

type jobRepo struct {
	dbConn *database.DbConnection
}

func NewJobRepo(dbConn *database.DbConnection) JobRepo {
	return &jobRepo{dbConn}
}

type JobRepo interface {
	GetEmpTypes() ([]model.EmploymentType, error)
	GetPosLevels() ([]model.PositionLevel, error)
	GetJobsList() ([]model.JobList, error)
	GetJobDetail(id string) (model.JobDetail, error)
}

func (r *jobRepo) GetEmpTypes() ([]model.EmploymentType, error) {
	empTypes := make([]model.EmploymentType, 0)
	db := r.dbConn.DB

	query := `SELECT emp_type_id, emp_type_name
	FROM public.employment_types`
	err := db.Raw(query).Scan(&empTypes).Error
	return empTypes, err
}

func (r *jobRepo) GetPosLevels() ([]model.PositionLevel, error) {
	posLevels := make([]model.PositionLevel, 0)
	db := r.dbConn.DB

	query := `SELECT pos_level_id, pos_level_name
	FROM public.position_levels`
	err := db.Raw(query).Scan(&posLevels).Error
	return posLevels, err
}
func (r *jobRepo) GetJobsList() ([]model.JobList, error) {
	jobs := make([]model.JobList, 0)
	db := r.dbConn.DB

	query := `SELECT job_id, job_name, job_desc
	FROM public.jobs`
	err := db.Raw(query).Scan(&jobs).Error
	return jobs, err
}
func (r *jobRepo) GetJobDetail(id string) (model.JobDetail, error) {
	job := model.JobDetail{}
	db := r.dbConn.DB

	query := `SELECT job_id, job_name, job_desc, job_min_qualifications, job_min_experiences, job_skill, job_benefit, pos_level_name, emp_type_name
	FROM public.jobs j
	left join public.position_levels pl on j.pos_level_id = pl.pos_level_id 
	left join public.employment_types et on j.emp_type_id = et.emp_type_id
	WHERE job_id=?::uuid;`
	err := db.Raw(query, id).Scan(&job).Error
	return job, err
}
